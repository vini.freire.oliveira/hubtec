require 'rails_helper'

RSpec.describe TaskPolicy, type: :policy do
  before(:each) do
    @admin = FactoryBot.create(:user)
    @admin.user_type = :admin
    @ordinary = FactoryBot.create(:user)
  end

  let(:user) { FactoryBot.create(:user) }
  let(:ordinary) { TaskPolicy.new(@ordinary, user) }
  let(:admin) { TaskPolicy.new(@admin, user) }

  permissions :index? do
    context "User show all tasks" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:index?)
      end
  
      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:index?)
      end
    end
  end

  permissions :show? do
    context "User show his tasks" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:show?)
      end

      it "grants access if user is an manager" do
        expect(admin).to permitted_to(:show?)
      end

    end
  end

  permissions :create? do
    context "User update his tasks" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:create?)
      end

      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:create?)
      end
    end
    
  end

  permissions :update? do
    context "User update his tasks" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:update?)
      end

      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:update?)
      end
    end
    
  end

  permissions :delete? do
    context "User update his tasks" do
      it "grants access if user is an ordinary" do
        expect(ordinary).to permitted_to(:update?)
      end

      it "grants access if user is an admin" do
        expect(admin).to permitted_to(:update?)
      end
    end
    
  end

end
