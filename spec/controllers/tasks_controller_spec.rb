require 'rails_helper'

RSpec.describe TasksController, type: :controller do

    describe "GET #index" do
        context "returns http success" do
            it "when ordinary user" do
                user = create(:user)
                sign_in user
                get :index
                expect(response).to have_http_status(:success)
            end

            it "when admin user" do
                user = create(:user)
                count = Task.all.count
                create(:task)
                create(:task)
                user.user_type = "admin"
                sign_in user
                get :index
                expect(response).to have_http_status(:success)
                expect(Task.all.count).to eql(count + 2)
            end
        end
    end

    describe "GET #show" do
        context "returns http success" do
            it "when ordinary user" do
                user = create(:user)
                sign_in user
                task = create(:task)
                get :show, params: { id: task.id }
                expect(response).to have_http_status(:success)
            end

            it "when admin user" do
                user = create(:user)
                user.user_type = "admin"
                sign_in user
                task = create(:task)
                get :show, params: { id: task.id }
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "Post #create" do
        context "returns http found" do
            it "when ordinary user" do
                user = create(:user)
                sign_in user
                post :create, params: {task: attributes_for(:task)}
                expect(response).to have_http_status(:found)
                expect(flash[:notice]).to match("Task was successfully created")
            end
        end

        context "returns http found" do
            it "when admin user" do
                user = create(:user)
                user.user_type = "admin"
                sign_in user
                post :create, params: {task: attributes_for(:task)}
                expect(response).to have_http_status(:found)
                expect(flash[:notice]).to match("Task was successfully created")
            end
        end
    end

    describe "Put #update" do
        context "returns http found" do
            it "when ordinary user" do
                user = create(:user)
                sign_in user
                task = create(:task)
                put :update, params: { id: task.id, task: attributes_for(:task)}
                expect(response).to have_http_status(:found)
                expect(flash[:notice]).to match("Task was successfully updated")
            end
        end

        context "returns http found" do
            it "when admin user" do
                user = create(:user)
                user.user_type = "admin"
                sign_in user
                task = create(:task)
                put :update, params: { id: task.id, task: attributes_for(:task)}
                expect(response).to have_http_status(:found)
                expect(flash[:notice]).to match("Task was successfully updated")
            end
        end
    end

    describe "Delete #destroy" do
        context "returns http found" do
            it "when ordinary user" do
                user = create(:user)
                sign_in user
                task = create(:task)
                delete :destroy, params: { id: task.id }
                expect(response).to have_http_status(:found)
                expect(flash[:notice]).to match("Task was successfully destroyed")
            end
        end

        context "returns http found" do
            it "when admin user" do
                user = create(:user)
                user.user_type = "admin"
                sign_in user
                task = create(:task)
                delete :destroy, params: { id: task.id }
                expect(response).to have_http_status(:found)
                expect(flash[:notice]).to match("Task was successfully destroyed")
            end
        end
    end

end
