## HubTec Test App

##### About

The system is a webapp, developed in RoR, where users can manage tasks: create, edit, update and delete (soft delete) and there is admin interface where users, with admin status, can manage every tasks, collect some results and export into different file types.

##### Operation

- <a href="https://hebtec-vinicius.herokuapp.com"> Heroku HubTec Teste Website</a>
  - Log in as admin: vini@email.com and password: 12345678
- Continuous integration with codeship, heroku and gitlab
  - Branch Master
  - Pipeline to exec Rspec Tests

#####  System dependencies

- Docker (<https://www.docker.com/>)
- Docker-compose
- Yarn

#####  Gems

- gem 'devise' multi-client and secure token-based authentication for Rails.
- gem 'raiils-admin' simple, RailsAdmin is a Rails engine that provides an easy-to-use interface for managing your data.
- gem 'pundit' to build a simple, robust and scaleable authorization system
- gem 'brakeman' brakeman is a security scanner for Ruby on Rails applications.
  - The brakeman results is in project root as <a href="/report-security-brakeman.html">report-security-brakeman.html</a>
- gem 'rack-cors' for handling Cross-Origin Resource Sharing (CORS)
- gem 'rack-attack' Rack middleware for blocking & throttling abusive requests
- The following gems were used to help in the automated tests 
  - Rspec
  - ffaker
  - factory_bot_rails
  - The Rspec results is in project root as <a href="/rspec-output-result.html">rspec-output-results.html</a>

#####  Step by step to run the project in localhost using docker-compose

1. Download the project from wetransfer

   ```bash
   git clone https://gitlab.com/vini.freire.oliveira/hubtec.git
   ```

2. Open the project folder

   ```bash
   cd hubtec
   ```

3. Build the containers and install the dependecies

   ```bash
   docker-compose run --rm app bundle install
   ```

4. Add jquery lib 

   ```bash
   docker-compose run --rm app bundle exec yarn add jquery
   ```

5. Add fontawesome styles

   ```bash
   docker-compose run --rm app bundle exec yarn add @fortawesome/fontawesome-free
   ```

6. Now, create database, generate migrations and run the seeds file:

   ```bash
   docker-compose run --rm app bundle exec rails db:create db:migrate
   ```

7. Start the server with

   ```bash
   docker-compose up
   ```

8. Open browser in Localhost:3000. The first user register will be the admin.

   

#####  To run Rspec test results

1. To run the requests spec

   ```
   docker-compose run --rm app bundle exec rspec spec/requests/
   ```

2. To run all spec files

   ```
   docker-compose run --rm app bundle exec rspec spec
   ```

3. To run and save all spec files

   ```
   docker-compose run --rm app bundle exec rspec spec/ --format h > output_test.html
   ```

#####  To run Breakman report results

1. To generate the report

   ```
   docker-compose run --rm app bundle exec brakeman -f html >> report.html
   ```

###  Links

#####  [My Git](https://gitlab.com/vini.freire.oliveira) 

#####  [My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/) 



#####  Extra docker-compose commands

1. Start the container in shared folder

   ```bash
   docker-compose run --rm app bash
   ```

2. Start the ruby container in ruby console

   ```bash
   docker-compose run --rm app bundle exec rails c
   ```