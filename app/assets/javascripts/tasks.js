$(document).on('turbolinks:load', function() {
    $("#showModal").click(function() {
        $(".modal").addClass("is-active");  
    });
    
    $("#closeModal").click(function() {
        $(".modal").removeClass("is-active");
    });
});