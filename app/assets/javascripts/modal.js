$(document).on('turbolinks:load', function() {

    modals = document.getElementsByClassName("modal-card-body");

    function setHeight() {
        for(var i = 0; i < modals.length; i++) {
            if(modals[i]) {
                modals[i].style.maxHeight = (window.innerHeight - 200) + 'px';
            }
        }
    }

    setHeight();

    window.onresize = function() {
        setHeight();
    };

});
