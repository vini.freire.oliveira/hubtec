function openNav() {
    document.getElementById("mySidenav").style.width = "80%";
    document.getElementById("mySidenav").style.minWidth="220px";
    document.getElementById("mySidenav").style.maxWidth="320px";
    if(isDesktop()){
        setTimeout(function () {
            let width = "" + document.getElementById("mySidenav").offsetWidth + "px";
            document.getElementById("dashboard").style.marginLeft = width;
        }, 150);
    }
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySidenav").style.minWidth="0";
    if(isDesktop()){
        document.getElementById("dashboard").style.marginLeft = "0px";
    }
}

function isDesktop() {
    if ($(window).width() > $(window).height()){
        return true; // desktop
    }else{
        return false; 
    }
 }
