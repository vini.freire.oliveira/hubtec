class DashboardController < ApplicationController
  before_action :authenticate_user!, only: [:index]
  before_action :set_tasks, only: [:index]
  
  def index
  end

  def welcome
  end

  private
  
  def set_tasks
    @tasks = current_user.tasks.find_all_valid.where(status: :created, status: :development).order("end_time ASC")
  end
end
