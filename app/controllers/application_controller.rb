class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
    include Pundit

    private

    def user_not_authorized
        redirect_to main_app.root_path, status: :unauthorized
    end

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :nickname, :avatar])
        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :nickname, :avatar])
    end


end
