class TasksController < ApplicationController
    before_action :authenticate_user!
    before_action :authorize_user
    before_action :set_task, only: [:show, :edit, :update, :destroy]

    
    def index
        @task = Task.new
        @tasks = current_user.tasks.find_all_valid.order("end_time ASC")
    end

    def show
    end

    def edit
    end

    def create
        @task = Task.new(task_params.merge(user: current_user))
        if @task.save
            redirect_to @task, notice: 'Task was successfully created'
        else
            render :index, notice: 'You are not able to do it'
        end
    end

    def update
        if @task.update(task_params)
            redirect_to @task, notice: 'Task was successfully updated'
        else
            render :edit, notice: 'You are not able to do it'
        end
    end

    def destroy
        if @task.soft_delete
            redirect_to tasks_url, notice: 'Task was successfully destroyed'
        else
            redirect_to @task, notice: 'You are not able to do it'
        end
    end

    private

    def set_task
        @task = Task.find(params[:id])
    end

    def authorize_user
        authorize Task
    end

    def task_params
        params.require(:task).permit(:title, :description, :feedback, :feedback, :link, :end_time, :status)
    end

    
end
