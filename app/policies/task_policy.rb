class TaskPolicy < ApplicationPolicy

  def index?
    user.admin? || user.ordinary?
  end

  def show?
    user.admin? || user.ordinary?
  end

  def edit?
    user.admin? || user.ordinary?
  end

  def update?
    user.admin? || user.ordinary?
  end

  def create?
    user.admin? || user.ordinary?
  end

  def destroy?
    user.admin? || user.ordinary?
  end
  

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
