class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  has_one_attached :avatar

  has_many :tasks

  validates :name, :nickname, presence: true
  enum user_type: [:admin, :ordinary]

  before_validation :set_user, on: :create

  private

  def set_user
    self.user_type = :admin if User.all.count == 0
    self.user_type = :ordinary if User.all.count > 0
  end


end
